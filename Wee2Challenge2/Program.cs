﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Wee2Challenge2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Enter array index :");
            int n = Convert.ToInt32(Console.ReadLine());
            int[] A = new int[n];
            Console.WriteLine("Please Enter ", +n, " Floating numbers");
            for (int i = 0; i < n; i++)
            {
                A[i] = Convert.ToInt32(Console.ReadLine());
            }
            int unpaired = Solution(A);
            Console.WriteLine("The unpaired number is :" + unpaired);
            Console.ReadLine();
        }

        public static int Solution(int[] A)
        {
            var result = A.Aggregate(0, (a, b) => a ^ b);
            return result;           
        }
    }
}
