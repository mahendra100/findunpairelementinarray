using System;
using Wee2Challenge2;
using Xunit;

namespace XUnitTest
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            int[] A = { 9, 3, 9, 3, 9, 7, 9 };
            Assert.Equal(7, Program.Solution(A));
        }
    }
}
